import hashlib
import time
# Start your middleware class
class ProxyMiddleware(object):
    # overwrite process request
    def process_request(self, request, spider):
        # Set the location of the proxy
        request.meta['proxy'] = "http://101.200.81.102:8123"
        appkey=53000891
        secret="d6f911c7e5398d251e2a290e7890f092"
        paramMap = {"app_key": appkey,"timestamp": time.strftime("%Y-%m-%d %H:%M:%S")}
        keys = paramMap.keys()
        keys.sort()
        codes= "%s%s%s" % (secret,str().join('%s%s' % (key, paramMap[key]) for key in keys),secret)
        sign = hashlib.md5(codes).hexdigest().upper()
        paramMap["sign"] = sign
        keys = paramMap.keys()
        authHeader = "MYH-AUTH-MD5 " + str('&').join('%s=%s' % (key, paramMap[key]) for key in keys)
        request.headers['Proxy-Authorization'] = authHeader
        #print authHeader
